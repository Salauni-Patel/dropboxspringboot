package com.springbootdropbox.SpringbootDropbox;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.*;
public class TestController extends SpringbootDropboxApplicationTests {

	@Autowired
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void testsignup() throws Exception {
		mockMvc.perform(post("/user/signup")).andExpect(status().isOk());

	}


	@Test
	public void testsignin() throws Exception {
		mockMvc.perform(post("/user/signin")).andExpect(status().isNotFound());

	}
	

	@Test
	public void testChecksession() throws Exception {
		mockMvc.perform(post("/user/checksession")).andExpect(status().isNotFound());

	}


	@Test
	public void testload() throws Exception {
		mockMvc.perform(post("/userfolder/load")).andExpect(status().isNotFound());

	}


	@Test
	public void testroot() throws Exception {
		mockMvc.perform(post("/userfolder/root")).andExpect(status().isNotFound());

	}


	@Test
	public void testCreatefolder() throws Exception {
		mockMvc.perform(post("/userfolder/createfolder")).andExpect(status().isNotFound());

	}


	@Test
	public void testupload() throws Exception {
		mockMvc.perform(post("/userfolder/upload")).andExpect(status().isNotFound());

	}


	@Test
	public void testshare() throws Exception {
		mockMvc.perform(post("/user/share")).andExpect(status().isNotFound());

	}


	@Test
	public void testedit() throws Exception {
		mockMvc.perform(post("/user/edit")).andExpect(status().isNotFound());

	}

}
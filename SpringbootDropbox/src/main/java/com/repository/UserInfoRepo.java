package com.repository;

import com.entity.UserInfo;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserInfoRepo extends CrudRepository<UserInfo, Integer> {



    UserInfo findUserByEmail(String email);
    UserInfo findUsersByEmailAndPassword(String email, String password);
}
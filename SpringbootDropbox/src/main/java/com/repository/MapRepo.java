package com.repository;


import com.entity.UserContent;
import com.entity.Map;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MapRepo  extends CrudRepository<Map,Integer> {

        List<Map> findMappingByFolderidAndUserid(int folderid, int userid);
}


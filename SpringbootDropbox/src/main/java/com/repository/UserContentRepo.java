package com.repository;

import com.entity.UserContent;
import com.entity.Map;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserContentRepo extends CrudRepository<UserContent,Integer> {
    List<UserContent> findAllByUserid(int userid);

    UserContent findAllByUseridAndOriginalname(int userid, String root);

    List<UserContent> findAllByContentidIn( List<Integer> contentid);

}
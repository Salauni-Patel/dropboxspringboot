package com.springbootdropbox.SpringbootDropbox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@SpringBootApplication
@EnableAutoConfiguration
public class SpringbootDropboxApplication {
	
	@Bean
	WebMvcConfigurer configurer () {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addResourceHandlers (ResourceHandlerRegistry registry) {
				registry.addResourceHandler("/pages/**").
						addResourceLocations("classpath:C:\\Users\\Patel\\Downloads\\SpringbootDropbox\\SpringbootDropbox\\src\\main\\resources\\static\\");
			}
		};
	}


	public static void main(String[] args) {
		SpringApplication.run(SpringbootDropboxApplication.class, args);
	}
}

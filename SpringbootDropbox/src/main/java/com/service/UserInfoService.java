package com.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.UserContent;
import com.entity.UserInfo;
import com.entity.Resp;
import com.entity.LoginResp;
import com.repository.UserContentRepo;
import com.repository.UserInfoRepo;

import java.util.Date;
import java.util.List;

@Service
public class UserInfoService {

	@Autowired
    private UserInfoRepo userRepository;
    @Autowired
    private UserContentRepo contentRepository;

    Resp response = new Resp();
    LoginResp signInResponse = new LoginResp();

    public LoginResp SignIn(UserInfo data){

        try{

            UserInfo user =  userRepository.findUsersByEmailAndPassword(data.getEmail(),data.getPassword());

            if(user==null){


                response.setStatus("error");
                response.setMsg("Email / Password may wrong.");
                signInResponse.setUsers(null);
                signInResponse.setResponse(response);
                return signInResponse;
            }
            else{

                response.setStatus("success");
                response.setMsg("Successfully Logged In with "+user.getEmail());
                signInResponse.setUsers(user);
                signInResponse.setResponse(response);
                return signInResponse;
            }


        }
        catch(Exception e){
            response.setStatus("error");
            response.setMsg("Something went wrong, Try Again.");
            signInResponse.setResponse(response);
            return signInResponse;
        }

    }

    public LoginResp CheckSession(UserInfo data){

        try{

            UserInfo user =  userRepository.findOne(data.getId());

            if(user==null){


                response.setStatus("error");
                response.setMsg("Please Sign In.");
                signInResponse.setUsers(null);
                signInResponse.setResponse(response);
                return signInResponse;
            }
            else{

                response.setStatus("success");
                response.setMsg("");
                signInResponse.setUsers(user);
                signInResponse.setResponse(response);
                return signInResponse;
            }


        }
        catch(Exception e){
            response.setStatus("error");
            response.setMsg("Something went wrong, Try Again.");
            signInResponse.setResponse(response);
            return signInResponse;
        }

    }


    public Resp SignUp(UserInfo data) {

        try {

            UserInfo users = userRepository.findUserByEmail(data.getEmail());
            data.setAll(data.getFirstname() + " " + data.getLastname() + " (" + data.getEmail() + ")");
            if (users == null) {
                users = userRepository.save(data);

                Date date = new Date();
                UserContent content = new UserContent();
                content.setOriginalname("root");
                content.setVirtualname("root");
                content.setStar("NO");
                content.setDate(date.toString());
                content.setUserid(users.getId());
                content.setType("folder");

                contentRepository.save(content);


                response.setStatus("success");
                response.setMsg("Account Created Successfully, Proceed to LogIn.");
                return response;
            } else {

                response.setStatus("error");
                response.setMsg("Account is already exist with email id: " + users.getEmail());
                return response;
            }


        } catch (Exception e) {
            response.setStatus("error");
            response.setMsg("Something went wrong, Try Again.");
            return response;
        }
    }


}

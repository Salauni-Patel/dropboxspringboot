package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.UserContent;
import com.entity.Map;
import com.entity.UserInfo;
import com.entity.UserDirectory;
import com.entity.ContentResp;
import com.entity.Resp;
import com.entity.RootResp;
import com.repository.UserContentRepo;
import com.repository.MapRepo;

import java.lang.invoke.ConstantCallSite;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserContentService {

	@Autowired
    private UserContentRepo contentRepository;
    @Autowired
    private MapRepo mappingRepository;


    public RootResp getRoot(UserInfo user){

        RootResp rootResponse = new RootResp();
        Resp response = new Resp();
        try{

            UserContent content =  contentRepository.findAllByUseridAndOriginalname(user.getId(),"root");
            rootResponse.setRootid(content.getContentid());
            response.setStatus("success");
            response.setMsg("");
            rootResponse.setResponse(response);

        }
        catch (Exception e){
            rootResponse.setRootid(0);
            response.setStatus("error");
            response.setMsg("Something went wrong.");
            rootResponse.setResponse(response);
        }
        return rootResponse;
    }
    public ContentResp getFolderData(UserDirectory folder){


        Resp response = new Resp();
        ContentResp contentLoadResponse = new ContentResp();

        try{



            // Get content

            List<Map> mapping = mappingRepository.findMappingByFolderidAndUserid(folder.getContentid(),folder.getUserid());

            List<Integer> contentid = mapping.stream().map(mapping1 -> mapping1.getContentid()).collect(Collectors.toList());

            contentRepository.findAllByContentidIn(contentid);

            contentLoadResponse.setContents(contentRepository.findAllByContentidIn(contentid));
            response.setStatus("success");
            response.setMsg("");
            contentLoadResponse.setResponse(response);
            contentLoadResponse.setParentfolderid(folder.getContentid());
       }
       catch (Exception e){

           response.setStatus("error");
           response.setMsg("Something went wrong");
           contentLoadResponse.setResponse(response);
           contentLoadResponse.setContents(null);
       }


        return contentLoadResponse;
    }

    public ContentResp UploadFile(String name, String path, int parentfolderid, int userid ){
        Resp response = new Resp();
        ContentResp contentLoadResponse = new ContentResp();
        Map mapping = new Map();

        try{

           // Add content start
            Date date = new Date();
            UserContent content = new UserContent();
            content.setOriginalname(name);
            content.setVirtualname(path);
            content.setStar("NO");
            content.setDate(date.toString());
            content.setUserid(userid);
            content.setType("file");

            content = contentRepository.save(content);

            // End

            // Mapping Start

            mapping.setContentid(content.getContentid());
            mapping.setFolderid(parentfolderid);
            mapping.setUserid(userid);
            mappingRepository.save(mapping);

            // Mapping End


            List<Map> mapping2 = mappingRepository.findMappingByFolderidAndUserid(parentfolderid,userid);

            List<Integer> contentid = mapping2.stream().map(mapping1 -> mapping1.getContentid()).collect(Collectors.toList());

            contentRepository.findAllByContentidIn(contentid);

            contentLoadResponse.setContents(contentRepository.findAllByContentidIn(contentid));
            response.setStatus("success");
            response.setMsg("File Successfully uploaded.");
            contentLoadResponse.setResponse(response);
            contentLoadResponse.setParentfolderid(parentfolderid);

        }
        catch (Exception e){

            response.setStatus("error");
            response.setMsg("Error in uploading, Please Try Again.");
            contentLoadResponse.setResponse(response);
            contentLoadResponse.setContents(null);

        }


        return contentLoadResponse;
    }

    public ContentResp CreateFolder(UserDirectory folder ){

        Resp response = new Resp();
        ContentResp contentLoadResponse = new ContentResp();
        Map mapping = new Map();

        try{

            // Add content start
            Date date = new Date();
            UserContent content = new UserContent();
            content.setOriginalname(folder.getFoldername());
            content.setVirtualname(folder.getFoldername());
            content.setStar("NO");
            content.setDate(date.toString());
            content.setUserid(folder.getUserid());
            content.setType("folder");

            content = contentRepository.save(content);

            // End

            // Mapping Start

            mapping.setContentid(content.getContentid());
            mapping.setFolderid(folder.getContentid());
            mapping.setUserid(folder.getUserid());
            mappingRepository.save(mapping);

            // Mapping End


            List<Map> mapping2 = mappingRepository.findMappingByFolderidAndUserid(folder.getContentid(),folder.getUserid());

            List<Integer> contentid = mapping2.stream().map(mapping1 -> mapping1.getContentid()).collect(Collectors.toList());

            contentRepository.findAllByContentidIn(contentid);

            contentLoadResponse.setContents(contentRepository.findAllByContentidIn(contentid));
            response.setStatus("success");
            response.setMsg("Folder Successfully Created.");
            contentLoadResponse.setResponse(response);
            contentLoadResponse.setParentfolderid(folder.getContentid());

        }
        catch (Exception e){

            response.setStatus("error");
            response.setMsg("Error in uploading, Please Try Again.");
            contentLoadResponse.setResponse(response);
            contentLoadResponse.setContents(null);

        }


        return contentLoadResponse;
    }

	
}

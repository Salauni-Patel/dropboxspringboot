package com.controller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.entity.UserInfo;
import com.entity.Resp;
import com.entity.LoginResp;
import com.service.UserInfoService;



@RestController   // This means that this class is a Controller
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(path="/user") // This means URL's start with /demo (after Application path)
public class UserInfoController {

	 @Autowired
	    private UserInfoService userService;



	    @RequestMapping(path="/signup",method = RequestMethod.POST) // Map ONLY POST Requests
	    public  Resp SignUp (@RequestBody UserInfo user) {


	        return  userService.SignUp(user);

	    }

	    @RequestMapping(path="/signin",method = RequestMethod.POST) // Map ONLY POST Requests
	    public LoginResp SignIn (@RequestBody UserInfo user) {


	        return userService.SignIn(user);

	    }

	    @RequestMapping(path="/checksession",method = RequestMethod.POST) // Map ONLY POST Requests
	    public LoginResp CheckSession (@RequestBody UserInfo user) {


	        return userService.CheckSession(user);

	    }



}

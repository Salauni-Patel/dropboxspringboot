package com.controller;

import com.entity.UserContent;
import com.entity.UserInfo;
import com.entity.UserDirectory;
import com.entity.ContentResp;
import com.entity.Resp;
import com.entity.RootResp;
import com.service.UserContentService;


import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;




@RestController    // This means that this class is a Controller
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(path="/userfolder") // This means URL's start with /userfolder (after Application path)
public class UserContentController {
	
	@Autowired
    private UserContentService usercontentService;

    private static String UPLOADED_FOLDER = System.getProperty("user.dir")+ "/src/main/resources/static/";


    @RequestMapping(path="/load",method = RequestMethod.POST)
    public ContentResp getFolderData(@RequestBody UserDirectory folder) {
        // This returns a JSON with the users
        return usercontentService.getFolderData(folder);
    }

    @RequestMapping(path="/root",method = RequestMethod.POST)
    public RootResp getRoot(@RequestBody UserInfo user) {
        // This returns a JSON with the users
        return usercontentService.getRoot(user);
    }
    @RequestMapping(path="/createfolder",method = RequestMethod.POST)
    public ContentResp CreateFolder(@RequestBody UserDirectory folder) {
        // This returns a JSON with the users
        return usercontentService.CreateFolder(folder);
    }

    @RequestMapping(path="/upload",method = RequestMethod.POST,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ContentResp fileupload(@RequestParam("file") MultipartFile multipartFile,
                                          @RequestParam("fileparent") String fileparent,
                                          @RequestParam("userid") String userid){

       // String email = (String) session.getAttribute("email");

        UserContent content = new UserContent();
        Date date = new Date();
        String virtualname= date+"_"+ multipartFile.getOriginalFilename();
        String filepath = UPLOADED_FOLDER + virtualname;
        Resp response = new Resp();
        ContentResp contentResp = new ContentResp();
        try {



            byte[] bytes = multipartFile.getBytes();
            Path path = Paths.get(filepath);
            Files.write(path, bytes);

            contentResp = usercontentService.UploadFile(multipartFile.getOriginalFilename(),
                    virtualname,Integer.parseInt(fileparent),Integer.parseInt(userid));


        } catch (IOException e) {
            response.setStatus("error");
            response.setMsg("Error in uploading, Please Try Again.");
            contentResp.setContents(null);
            contentResp.setResponse(response);
        }


        return contentResp;
           }




}

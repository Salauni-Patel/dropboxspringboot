package com.entity;

import com.entity.UserInfo;
import com.entity.Resp;

public class LoginResp {

	Resp response;
    UserInfo users;

    public Resp getResponse() {
        return response;
    }

    public void setResponse(Resp response) {
        this.response = response;
    }

    public UserInfo getUsers() {
        return users;
    }

    public void setUsers(UserInfo users) {
        this.users = users;
    }

}

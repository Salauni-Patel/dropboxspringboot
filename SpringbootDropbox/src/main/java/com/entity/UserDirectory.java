package com.entity;

public class UserDirectory {

	 int userid;
	    int contentid;
	    String  foldername;

	    public String getFoldername() {
	        return foldername;
	    }

	    public void setFoldername(String foldername) {
	        this.foldername = foldername;
	    }

	    public int getUserid() {
	        return userid;
	    }

	    public void setUserid(int userid) {
	        this.userid = userid;
	    }

	    public int getContentid() {
	        return contentid;
	    }

	    public void setContentid(int contentid) {
	        this.contentid = contentid;
	    }

}

package com.entity;
import com.entity.UserContent;

import com.entity.Resp;

import java.util.List;



public class ContentResp {


    List<UserContent> contents;
    Resp response;
    int parentfolderid;

    public List<UserContent> getContents() {
        return contents;
    }

    public void setContents(List<UserContent> contents) {
        this.contents = contents;
    }

    public Resp getResponse() {
        return response;
    }

    public void setResponse(Resp response) {
        this.response = response;
    }

    public int getParentfolderid() {
        return parentfolderid;
    }

    public void setParentfolderid(int parentfolderid) {
        this.parentfolderid = parentfolderid;
    }

}

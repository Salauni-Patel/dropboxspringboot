var React = require('react');
var ReactDOM = require('react-dom');
import App from "./components/app";



ReactDOM.render(
        <App/>
    , document.getElementById('app'));

$(window).resize(function () {
    var path = $(this);
    var contW = path.width();
    if (contW >= 751) {
        document.getElementsByClassName("main-content")[0].style.marginLeft = "200px";
        document.getElementsByClassName("sidebar-toggle")[0].style.left = "200px";
    } else {
        document.getElementsByClassName("main-content")[0].style.marginLeft = "0px";
        document.getElementsByClassName("sidebar-toggle")[0].style.left = "-200px";
    }
});
$(document).ready(function () {
    var loggedInUserEmail = sessionStorage.getItem("loggedInUserEmail");
    if (loggedInUserEmail && loggedInUserEmail.length > 0) {
        $('[href="/login"]').hide()
        $('[href="/signup"]').hide()
        $('[href="/logout"]').show()
        $('[id="sidebar-wrapper"]').show()
        document.getElementsByClassName("main-content")[0].style.marginLeft = "200px";

    } else {
        $('[href="/login"]').show()
        $('[href="/signup"]').show()
        $('[href="/logout"]').hide()
        $('[id="sidebar-wrapper"]').hide()
        document.getElementsByClassName("main-content")[0].style.marginLeft = "0px";

    }

    $('.dropdown').on('show.bs.dropdown', function (e) {
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown(300);
    });
    $('.dropdown').on('hide.bs.dropdown', function (e) {
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp(300);
    });
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        var elem = document.getElementById("sidebar-wrapper");
        var left = window.getComputedStyle(elem, null).getPropertyValue("left");
        if (left == "200px") {
            document.getElementsByClassName("sidebar-toggle")[0].style.left = "-200px";
        }
        else if (left == "-200px") {
            document.getElementsByClassName("sidebar-toggle")[0].style.left = "200px";
        }
    });
});
